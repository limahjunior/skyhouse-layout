var SHI = {};

SHI.Layouts = {};

SHI.Layouts.One = function () {
    return '<div class="f-page">\
              <div class="f-title">\
              <a href="index.html">P�gina Anterior</a>\
              <h2>SkyHouse Incubator</h2>\
              <a href="index.html?page=2">P�gina Seguinte</a>\
           </div>\
           <div class="box w-25 h-70">\
               <div class="img-cont img-1"></div>\
               <h3>{{title}} <span>{{date}}</span></h3>\
               <p>{{article}}</p>\
           </div>\
           <div class="box w-50 h-70 box-b-l box-b-r">\
               <div class="img-cont img-2"></div>\
               <h3>{{title}} <span>{{date}}</span></h3>\
               <p>{{article}}</p>\
           </div>\
           <div class="box w-25 h-70">\
               <div class="img-cont img-3"></div>\
               <h3>{{title}} <span>{{date}}</span></h3>\
               <p>{{article}}</p>\
           </div>\
           <div class="box w-50 h-30 box-b-r title-top">\
               <h3>{{title}} <span>{{date}}</span></h3>\
               <p>{{article}}</p>\
           </div>\
           <div class="box w-50 h-30 title-top">\
               <h3>{{title}} <span>{{date}}</span></h3>\
               <p>{{article}}</p>\
           </div>\
        </div>';
};