$(document).ready(function () {
    var items = ["1", "2", "3", "4", "5"];
    //alert(items[Math.floor(Math.random()*items.length)]);
    alert(pickRandomProperty(items));

    //$("div[mode]")

    /*
    primeiro puxa os artigos
    depois conta quantos vieram para come�ar a estudar a distribui��o nas p�ginas
    o m�ximo de artigos por p�gina ser� 6
    depois de definir o layout, verificar quais artigos cont�m imagem
    ap�s definir a quantidade de artigos da p�gina, selecionar o layout aleatoriamente, por exemplo uma p�gina com 4 artigos:
        (60)|50|-|50|   ou   (50)|75|-|25|
        (40)|50|-|50|        (50)|45|-|55|
    seguindo esta premissa, teremos sempre 6 aleatoriedades poss�veis
    uma op��o para resolver a quest�o dos layouts, seria padronizar a mesma sequ�ncia de layout para todos os usu�rios, mudando apenas e obviamente, a fonte dos artigos
    */
});

function pickRandomProperty(obj) {
    var result;
    var count = 0;
    for (var prop in obj)
        if (Math.random() < 1 / ++count)
            result = prop;
    return result;
}

/*#TP1
(70)|25|-|50|-|25|
(30)|50|-|50|
*/
var tp1 = '<div class="f-page">\
              <div class="f-title">\
              <a href="index.html">P�gina Anterior</a>\
              <h2>SkyHouse Incubator</h2>\
              <a href="index.html?page=2">P�gina Seguinte</a>\
           </div>\
           <div class="box w-25 h-70">\
               <div class="img-cont img-1"></div>\
               <h3>{{title}} <span>{{date}}</span></h3>\
               <p>{{article}}</p>\
           </div>\
           <div class="box w-50 h-70 box-b-l box-b-r">\
               <div class="img-cont img-2"></div>\
               <h3>{{title}} <span>{{date}}</span></h3>\
               <p>{{article}}</p>\
           </div>\
           <div class="box w-25 h-70">\
               <div class="img-cont img-3"></div>\
               <h3>{{title}} <span>{{date}}</span></h3>\
               <p>{{article}}</p>\
           </div>\
           <div class="box w-50 h-30 box-b-r title-top">\
               <h3>{{title}} <span>{{date}}</span></h3>\
               <p>{{article}}</p>\
           </div>\
           <div class="box w-50 h-30 title-top">\
               <h3>{{title}} <span>{{date}}</span></h3>\
               <p>{{article}}</p>\
           </div>\
        </div>';